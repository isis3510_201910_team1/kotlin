package co.edu.uniandes.svvi

import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager

class AlertaNoInternet(contexto: Context) {

    var contexto: Context? = null

    init {
        this.contexto = contexto
    }

    fun dialogoAlerta(): AlertDialog{
        val builder :AlertDialog.Builder = AlertDialog.Builder(contexto)

        val nombreBoton = contexto?.getResources()?.getString(R.string.but_tap_to_record)

        var mensaje = "Lo sentimos, pero en estos momentos no se puede utilizar esta función ya que depende de una conexión a nuestros servidores.\n"
        mensaje += "Por favor, revisa tu conexión a internet e inténtalo más tarde.\n"
        mensaje += "De momento te invitamos a que escribas tu mensaje en el campo que se encuentra arriba del botón \"$nombreBoton\"."

        builder.setTitle("No se puede usar esta función en estos momentos")
            .setMessage(mensaje)
            .setPositiveButton("De acuerdo.", null)  //No se hace nada al aceptar y por eso se pasa un null.
        return builder.create()
    }

    fun revisarInternet(): Boolean{
        val manejoConexion: ConnectivityManager = contexto?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activa = manejoConexion.activeNetworkInfo
        val hayInternet = activa != null && activa.isConnected

        return hayInternet
    }
}