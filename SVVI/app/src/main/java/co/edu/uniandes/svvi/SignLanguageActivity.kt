package co.edu.uniandes.svvi

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.camerakit.CameraKitView
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ml.common.modeldownload.FirebaseLocalModel
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager
import com.google.firebase.ml.common.modeldownload.FirebaseRemoteModel
import com.google.firebase.ml.custom.*
import java.io.*
import android.os.Handler
import android.view.MotionEvent
import android.view.View.OnClickListener
import android.view.View.OnTouchListener
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SignLanguageActivity : AppCompatActivity() {

    //Constant to request image capture to camera
    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_GET_SINGLE_FILE = 3

    //Take a picture button
    private var butTakePicture: ImageButton? = null

    //Image view to show pictures
    private var imageView: ImageView? = null

    //Text view to show results
    private var textView: TextView? = null

    //Current photo path to save to file
    private var currentPhotoPath: String? = null

    //Picture bitmap
    private lateinit var photoImage: Bitmap

    //Boolean for stopping subsequent labels
    private var alreadyLabeled:Boolean = false

    //Database reference for updating model
    private lateinit var firebaseDB:DatabaseReference

    //Camera library variables
    private var cameraKitView: CameraKitView? = null

    //All interpretations String
    private var allResults: String? = ""

    @SuppressLint("MissingPermission", "ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupPermissions()

        setContentView(R.layout.activity_sign_language)

        //imageView = findViewById(R.id.image_view)

        textView = findViewById(R.id.text_gesture_result)

        butTakePicture = findViewById(R.id.but_take_picture)

        butTakePicture?.setOnTouchListener(RepeatListener(5000, 5000, OnClickListener {
            cameraKitView?.captureImage { _: CameraKitView, bytes: ByteArray ->
                GlobalScope.launch {
                    runInference(
                        bitmapToInputArray(
                            BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
                        )
                    )
                }
            }
        }))
        //if(alreadyLabeled){
        //    alreadyLabeled = false
        //    textView?.text = " "
        //}
        //val values = ContentValues(1)
        //values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
        //val fileUri = contentResolver
        //    .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
        //        values)
        //val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        //if(intent.resolveActivity(packageManager) != null) {
        //    currentPhotoPath = fileUri.toString()
        //   intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        //   intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
        //           or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        //   startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
        //}
        //Camera Library Variables
        cameraKitView = findViewById(R.id.camera)
        //Firebase Db
        firebaseDB = FirebaseDatabase.getInstance().reference

        //Hosted Models
        createModels()
    }

    @SuppressLint("MissingPermission")
    override fun onStart() {
        super.onStart()
        cameraKitView?.onStart()
    }

    override fun onResume() {
        super.onResume()
        cameraKitView?.onResume()
    }

    override fun onPause() {
        cameraKitView?.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        cameraKitView?.onStop()
        super.onDestroy()
    }

    private fun createModels() {
        //Hosted Model
        var conditionsBuilder: FirebaseModelDownloadConditions.Builder =
        FirebaseModelDownloadConditions.Builder().requireWifi()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // Enable advanced conditions on Android Nougat and newer.
            conditionsBuilder = conditionsBuilder
                .requireCharging()
                .requireDeviceIdle()
        }
        val conditions = conditionsBuilder.build()

        // Build a remote model object by specifying the name you assigned the model
        // when you uploaded it in the Firebase console.
        val cloudSource = FirebaseRemoteModel.Builder("my_cloud_model")
            .enableModelUpdates(true)
            .setInitialDownloadConditions(conditions)
            .setUpdatesDownloadConditions(conditions)
            .build()
        FirebaseModelManager.getInstance().registerRemoteModel(cloudSource)

        //Local Model
        val localSource = FirebaseLocalModel.Builder("my_local_model") // Assign a name to this model
            .setAssetFilePath("optimized_graph_signs.tflite")
            .build()
        FirebaseModelManager.getInstance().registerLocalModel(localSource)

    }

    private fun setupPermissions() {
        val externalSPermission = ContextCompat.checkSelfPermission(this,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (externalSPermission != PackageManager.PERMISSION_GRANTED) {
            Log.i("Permission denied", "Permission to write external storage denied")
            makeRequest(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, 101)
        }

        val cameraPermission = ContextCompat.checkSelfPermission(this,
            android.Manifest.permission.CAMERA)

        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            Log.e("Permission denied", "Permission to record denied")
            makeRequest(android.Manifest.permission.CAMERA, 100)
        }
    }

    private fun makeRequest(manifestPermission: String, requestCode: Int) {
        ActivityCompat.requestPermissions(this,
            arrayOf(manifestPermission),
            requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            try {
                val stream = contentResolver!!.openInputStream(Uri.parse(currentPhotoPath))
                if (::photoImage.isInitialized) photoImage.recycle()
                photoImage = BitmapFactory.decodeStream(stream)
                imageView?.setImageBitmap(photoImage)

                val input:Array<Array<Array<FloatArray>>> = bitmapToInputArray(photoImage)

                runInference(input)

            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
        }
        if (requestCode == REQUEST_GET_SINGLE_FILE) {
            val selectedImageUri = data?.data
            // Get the path from the Uri
            var path:String? = null
            val prod = arrayOf(MediaStore.Images.Media.DATA)
            val cursor = contentResolver.query(selectedImageUri, prod, null, null, null)
            if (cursor!!.moveToFirst()) {
                val column = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                path = cursor.getString(column)
            }
            cursor.close()
            if (path != null) {
                currentPhotoPath = path
            }
        }
    }

    private fun bitmapToInputArray(myImage:Bitmap):Array<Array<Array<FloatArray>>>{
        val bitmap = Bitmap.createScaledBitmap(myImage, 224, 224, true)

        val batchNum = 0
        val input = Array(1) { Array(224) { Array(224) { FloatArray(3) } } }
        for (x in 0..223) {
            for (y in 0..223) {
                val pixel = bitmap.getPixel(x, y)
                input[batchNum][x][y][0] = (Color.red(pixel) - 127) / 128.0f
                input[batchNum][x][y][1] = (Color.green(pixel) - 127) / 128.0f
                input[batchNum][x][y][2] = (Color.blue(pixel) - 127) / 128.0f
            }
        }
        return input
    }

    private fun runInference(input:Array<Array<Array<FloatArray>>>){
        //Interpreter
        val options = FirebaseModelOptions.Builder()
            .setRemoteModelName("my_cloud_model")
            .setLocalModelName("my_local_model")
            .build()
        val interpreter = FirebaseModelInterpreter.getInstance(options)

        //Setup Input Options
        val inputOutputOptions = FirebaseModelInputOutputOptions.Builder()
            .setInputFormat(0, FirebaseModelDataType.FLOAT32, intArrayOf(1, 224, 224, 3))
            .setOutputFormat(0, FirebaseModelDataType.FLOAT32, intArrayOf(1, 11))
            .build()

        val inputs = FirebaseModelInputs.Builder()
            .add(input) // add() as many input arrays as your model requires
            .build()
        interpreter?.run(inputs, inputOutputOptions)
            ?.addOnSuccessListener { result ->
                val output = result.getOutput<Array<FloatArray>>(0)
                val probabilities = output[0]
                var maxProbability = 0.0f
                var highLabel = "Unrecognized Image"

                val reader = BufferedReader(
                    InputStreamReader(assets.open("retrained_labels_signs.txt")))
                for (i in probabilities.indices) {
                    val label = reader.readLine()
                    Log.i("MLKit", String.format("%s: %1.4f", label, probabilities[i]))
                    if (probabilities[i] > maxProbability) {
                        maxProbability = probabilities[i]
                        highLabel = label
                    }
                }
                allResults = "$allResults $highLabel"
                Log.i("ALL RESULTS", allResults)
                textView?.text = allResults
            }
            ?.addOnFailureListener {
                Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                Log.e("ERROR", it.stackTrace!!.contentToString())
                Log.e("ERROR", it.localizedMessage)
                Log.e("ERROR", it.cause.toString())
            }
    }
}

//Helper classes for on touch listener
class RepeatListener( private val initialInterval: Int, private val normalInterval: Int, private val clickListener: OnClickListener?) : OnTouchListener {

    private val handler = Handler()

    private val handlerRunnable = object : Runnable {
        override fun run() {
            handler.postDelayed(this, normalInterval.toLong())
            clickListener?.onClick(downView)
        }
    }

    private var downView: View? = null

    init {
        if (clickListener == null)
            throw IllegalArgumentException("null runnable")
        if (initialInterval < 0 || normalInterval < 0)
            throw IllegalArgumentException("negative interval")
    }

    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        when (motionEvent.getAction()) {
            MotionEvent.ACTION_DOWN -> {
                handler.removeCallbacks(handlerRunnable)
                handler.postDelayed(handlerRunnable, initialInterval.toLong())
                downView = view
                downView!!.isPressed = true
                clickListener?.onClick(view)
                return true
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                handler.removeCallbacks(handlerRunnable)
                downView!!.isPressed = false
                downView = null
                return true
            }
        }
        return false
    }
}
