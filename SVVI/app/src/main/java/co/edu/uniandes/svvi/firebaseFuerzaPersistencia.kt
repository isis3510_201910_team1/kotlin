package co.edu.uniandes.svvi

import android.app.Application
import com.google.firebase.FirebaseApp
import com.google.firebase.database.FirebaseDatabase

class firebaseFuerzaPersistencia: Application() {

    override fun onCreate(){
        super.onCreate()
        if(!FirebaseApp.getApps(this).isEmpty()){
            FirebaseDatabase.getInstance().setPersistenceEnabled(true) //Se asegura de que la instancia de Firebase mantenga la persistencia en disco.

        }
    }
}