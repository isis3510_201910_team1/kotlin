package co.edu.uniandes.svvi

import android.Manifest
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.lang.Exception
import java.util.Calendar
import java.util.Date

class MenuActivity : AppCompatActivity() {

    private lateinit var firebaseDB: DatabaseReference
    private var conversationID: String? = null
    private var fechaActual:Date? = null
    private var diasSemana: Array<String> = arrayOf("Domingo",
                                                    "Lunes",
                                                    "Martes",
                                                    "Miércoles",
                                                    "Jueves",
                                                    "Viernes",
                                                    "Sábado")

    companion object {
        val STATSCOLLECTION: String = "stats"
        val ID: String = "id"
        val FECHA: String = "fecha"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        //Obtiene la ref a la db
        firebaseDB = FirebaseDatabase.getInstance().reference
        if (savedInstanceState != null){//Si esa instancia no es null, quiere decir que hay una conversacion
            conversationID = savedInstanceState.getString(ID) //se toma el id de esa conversacion
            val longFecha = savedInstanceState.getLong(FECHA) //se toma el long de la fecha
            fechaActual = Date(longFecha) //crea la fecha con el long.
        } else {
            //Si no es así, se esta creando una actividad de cero y en ese caso
            //se crea un nuevo objeto para almacenar la info de conversacion dentro de la db
            val nuevaConversacion = firebaseDB.child(STATSCOLLECTION).push()
            conversationID = nuevaConversacion.key //Obtiene el id de ese objeto.

            val calendario = Calendar.getInstance()
            fechaActual = calendario.time
            firebaseDB.child(STATSCOLLECTION).child(conversationID.toString())
                .child("fecha").setValue(fechaActual)//Guarda la fecha actual.

            val diaSemana: Int = calendario.get(Calendar.DAY_OF_WEEK)//Obtiene el dia de la semana, con domingo como el 1
            val diaActual: String = diasSemana[diaSemana - 1] //Obtiene el dia de la semana del arreglo.
            firebaseDB.child(STATSCOLLECTION).child(conversationID.toString())
                .child("dia").setValue(diaActual)//Guarda el dia de la semana en db

        } //Asi siempre se obtine el id de la conversacion actual.

        val email = intent.getStringExtra("userEmail")

        val textView = findViewById<TextView>(R.id.emailView)
        textView.text = email

        val btnSli = findViewById<Button>(R.id.btn_sli)
        val btnVs = findViewById<Button>(R.id.btn_vs)
        val btnLoginRegister = findViewById<Button>(R.id.but_login_register)

        btnSli.setOnClickListener {
            val intent = Intent(this, SignLanguageActivity::class.java)
            intent.putExtra(ID, conversationID)
            startActivity(intent)
        }

        btnVs.setOnClickListener {
            val intent = Intent(this, SubtitlesActivity::class.java)
            intent.putExtra(ID, conversationID)
            startActivity(intent)
        }

        btnLoginRegister.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            intent.putExtra(ID, conversationID)
            startActivity(intent)
        }
        checkCameraPermission()
    }

    @AfterPermissionGranted(100)
    private fun checkCameraPermission(){
        if(EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA)){
            Log.d("PERMISSION", "CAMERA PERMITTED")
        }else{
            EasyPermissions.requestPermissions(this, getString(R.string.camera_request_rationale),100, Manifest.permission.CAMERA)
        }
    }

    override fun onSaveInstanceState (outState: Bundle){
        outState.putString(ID, conversationID)
        try {
            outState.putLong(FECHA, fechaActual!!.time) //Almacena la fecha actual en caso de que se deba recuperar el estado de la app.
        } catch (e: Exception) {
            Log.d("Error de fecha nula:",e.message)
        }

        super.onSaveInstanceState(outState)
    }
}
