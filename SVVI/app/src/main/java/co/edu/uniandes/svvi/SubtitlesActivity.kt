package co.edu.uniandes.svvi

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.speech.tts.TextToSpeech
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.*
import co.edu.uniandes.svvi.MenuActivity.Companion.ID
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.util.*
import kotlin.collections.ArrayList


class SubtitlesActivity : AppCompatActivity(), TextToSpeech.OnInitListener, AdapterView.OnItemSelectedListener {

    private var tts: TextToSpeech? = null
    private var txtContent: TextView? = null
    private var txtRecorded: EditText? = null
    private var conversationID: String? = null //Id para la estadística de la app.

    //Toma los botones desde el principio para mejorar la lectura (y un poco la ejecucion) del código
    private var butTextToSpeech: Button? = null
    private var butSpeechToText: Button? = null
    private lateinit var firebaseDB: DatabaseReference

    private var mSpeechRecognizer: SpeechRecognizer? = null
    private val mSpeechRecognizerIntent: Intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)

    var numTTS: Int = 0
    var numSTT: Int = 0
    var localActual = Locale("es")

    companion object {
        val NUMTTS: String = "tts"
        val NUMSTT: String = "stt"
    }
    
    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subtitles)
        val yo = this

        if(savedInstanceState != null ){//Si esa instancia no es null, quiere decir que hay una conversacion
            conversationID = savedInstanceState.getString(ID) //se toma el id de esa conversacion
            numTTS = savedInstanceState.getInt(NUMTTS)
            numSTT = savedInstanceState.getInt(NUMSTT) //Obtiene las estadísticas previas.
        } else conversationID = intent.getStringExtra(ID)
        firebaseDB = FirebaseDatabase.getInstance().reference

        txtContent = findViewById(R.id.txt_content)

        tts = TextToSpeech(this, this)

        butTextToSpeech = findViewById(R.id.but_text2speech)
        butTextToSpeech?.setOnClickListener{
            speak()
        }
        //El text to speech necesita un tiempo para inicializarce y por lo tanto
        //el botón no puede estar habilitado sino hasta que termine de inicializar
        butTextToSpeech?.isEnabled = false

        //Speech to text
        setupPermissions()
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this)
        if(!SpeechRecognizer.isRecognitionAvailable(this)){
            Log.e("ERROR", "RECOGNITION SERVICE NOT AVAILABLE")
        }
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
            localActual.toString())
        mSpeechRecognizer?.setRecognitionListener(object: RecognitionListener{
            override fun onReadyForSpeech(p0: Bundle?) {
                Log.i("READY FOR SPEECH", p0?.toString())
            }

            override fun onRmsChanged(p0: Float) {
                val quietMax = 25f
                val mediumMax = 65f

                if (p0 < quietMax) {
                    Log.i("RMS CHANGED","Quiet$p0")
                    // quiet
                } else if (p0 >= quietMax && p0 < mediumMax) {
                    Log.i("RMS CHANGED","Medium$p0")
                    // medium
                } else {
                    Log.i("RMS CHANGED","Loud$p0")
                    // loud
                }
            }

            override fun onBufferReceived(p0: ByteArray?) {
                Log.i("BUFFER RECEIVED", p0?.toString())
            }

            override fun onPartialResults(p0: Bundle?) {
                Log.i("PARTIAL RESULTS", p0?.toString())
            }

            override fun onEvent(p0: Int, p1: Bundle?) {
                Log.i("EVENT", "event $p0 and ${p1.toString()}")
            }

            override fun onBeginningOfSpeech() {
                Log.i("BEGIN OF SPEECH", "Started recording audio")
            }

            override fun onEndOfSpeech() {
                Log.i("BEGIN OF SPEECH", "Started recording audio")
            }

            override fun onError(p0: Int) {
                Log.e("ERROR", p0.toString())

                when(p0) {
                    8 -> {
                        mSpeechRecognizer?.cancel()
                        Log.i("FIXED", "Fixed the 'error 8', listening again")
                    }
                }
            }

            override fun onResults(p0: Bundle?) {

                //getting all the matches
                val matches = p0?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)

                var texto = ""

                //displaying the first match
                if (matches != null){
                    texto = matches[0]
                    txtRecorded?.setText(texto)
                }

                val referenciadb = firebaseDB.child(MenuActivity.STATSCOLLECTION).child(conversationID.toString())

                referenciadb.child("numSTT").setValue(++numSTT) //sube ese valor a firebase

                val hijoCant = referenciadb.child("cantidades").push().key //Crea un nuevo objeto para almacenar las cantidades

                referenciadb.child("cantidades").child(hijoCant.toString()).child("caracteresSTT").setValue(texto.length)   //Almacena los caracteres de TTS

                val palabras = texto.split(" ").size
                referenciadb.child("cantidades").child(hijoCant.toString()).child("palabrasSTT").setValue(palabras)   //Almacena las palabras de TTS
            }

        })

        butSpeechToText = findViewById(R.id.but_speech2text)
        butSpeechToText?.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    //when the user removed the finger
                    mSpeechRecognizer?.stopListening()
                    txtRecorded?.hint = "Tu mensaje va a aparecer acá."
                }

                MotionEvent.ACTION_DOWN -> {
                    val alerta = AlertaNoInternet(yo)
                    if (alerta.revisarInternet()){
                        //finger is on the button
                        mSpeechRecognizer?.startListening(mSpeechRecognizerIntent)
                        txtRecorded?.setText("")
                        txtRecorded?.hint = "Escuchando..."
                    } else {
                        alerta.dialogoAlerta().show()   //Muestra el cuadro de alerta
                    }
                }
            }
            false
        }

        txtRecorded = findViewById<EditText>(R.id.content_speech2text)

        AjustarSpinner()
    }

    //Metodo para manejar el cambio del spinner
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when( position ){
            0 -> {
                localActual = Locale("es")
                butTextToSpeech?.isEnabled = false
                cambiarLocalTTS()
                mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                    localActual.toString())
            }
            1 -> {
                localActual = Locale("en")
                butTextToSpeech?.isEnabled = false
                cambiarLocalTTS()
                mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                    localActual.toString())
            }
        }
    }

    //Metodo necesario en la configuracion del spinner. Vacio porque no es necesariohacer algo
    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    //Se encarga de poner las opciones al "dropdown" o spinner de la interáz.
    fun AjustarSpinner(){
        val dropDown: Spinner = findViewById<Spinner>(R.id.spinner) //Referencia al spinner

        val idiomas: ArrayList<String> = ArrayList<String>()
        idiomas.add(0,"Español")
        idiomas.add("Inglés") //Lista con los idiomas y el Español como el por defecto

        val adaptador: ArrayAdapter<String> = ArrayAdapter(this, R.layout.spinner_item, idiomas)   //configura el adaptador y los elementos.
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)         //Configura el diseño del adaptador

        dropDown.adapter = adaptador

        dropDown.onItemSelectedListener = this  // Esta actividad se encarga del cambio del selector.
    }


    //Cambia el local del TTS y habilita el botón.
    fun cambiarLocalTTS(){
        val result = tts!!.setLanguage(localActual)

        if(result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED){
            Log.e("TTS","El lenguaje especificado no esta soportado")
        } else {
            //Activar el botón ahora ya que logró inicializar el tts
            butTextToSpeech?.isEnabled = true
        }
    }

    //Este metodo se agrega para la inicialización del componente tts
    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS){
           cambiarLocalTTS()
        } else {
            Log.e("TTS", "Initialization Failed!")
        }
    }

    private fun speak() {
        if (txtContent?.text.toString().trim().isEmpty()) {
            val builder = AlertDialog.Builder(this)
            builder.setCancelable(true)
            builder.setTitle("¡Atención!")
            builder.setMessage("No has ingresado ningún texto para decir en la aplicación. Por favor escribe algo e intenta de nuevo.")
            val dialog = builder.create()
            dialog.show()
        } else {
            val texto = txtContent?.text.toString() //toma el texto a hablar
            tts?.speak(texto, TextToSpeech.QUEUE_FLUSH, null, "")
            txtContent?.text = "" //Elimina el mensaje a hablar ya que este encuentra en cola.
            numTTS++ //Actualiza el valor de la cantidad de usos de text to speech

            val referenciadb = firebaseDB.child(MenuActivity.STATSCOLLECTION).child(conversationID.toString())

            referenciadb.child("numsTT").setValue(numTTS) //sube ese valor a firebase

            val hijoCant = referenciadb.child("cantidades").push().key //Crea un nuevo objeto para almacenar las cantidades

            referenciadb.child("cantidades").child(hijoCant.toString()).child("caracteresTTS").setValue(texto.length)   //Almacena los caracteres de TTS

            val palabras = texto.split(" ").size
            referenciadb.child("cantidades").child(hijoCant.toString()).child("palabrasTTS").setValue(palabras)   //Almacena las palabras de TTS
        }
    }

    override fun onSaveInstanceState (outState: Bundle){
        outState.putString(ID, conversationID)
        outState.putInt(NUMTTS,numTTS)
        outState.putInt(NUMSTT,numSTT)

        super.onSaveInstanceState(outState)
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
            android.Manifest.permission.RECORD_AUDIO)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i("Permission denied", "Permission to record denied")
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
            arrayOf(Manifest.permission.RECORD_AUDIO),
            101)
    }

    //tts debe morir por motivos de rendimiento cuando se cierra la actividad
    //Por eso es necesario hacer esto en el onDestroy()
    public override fun onDestroy() {
        if (tts != null) {
            tts!!.stop()
            tts!!.shutdown()
        }
        mSpeechRecognizer?.destroy()
        super.onDestroy()
    }
}
